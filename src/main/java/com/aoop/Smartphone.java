package com.aoop;

public class Smartphone {
    public String display;
    public String speaker;

    public Smartphone(String display, String speaker) {
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getSpeaker() {
        return speaker;
    }

    public void setSpeaker(String speaker) {
        this.speaker = speaker;
    }
}
